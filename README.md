# tezos_map

This is a project for tezos guide map inspired by '[Web Developer Roadmap].'  

[Web Developer Roadmap]: https://codeburst.io/the-2018-web-developer-roadmap-826b1b806e8d


## How it works
TBD  
Materials filtereb by   
1) questions and answers  
2) hash tags (eg. #ios, #smartcontract #dapp)  
3) ...

### by sujects
Official Document  
Development  
Foundations  
Communication  
Projects

### by stakeholders
#### developers
  iOS, Android, SmartContracts, Core, 개발자채널 riot slack, 개발자문서
#### researchers
#### bakers
  how to bake, monitoring, sending rewards, baking slack
#### end users
  wallets, how to use wallet, how to delegate
#### start-ups



## Links
Being updated.

### Official Document  
#### White paper
https://tezos.com/static/white_paper-2dc8c02267a8fb86bd67a108199441bf.pdf
#### Position paper
https://tezos.com/static/position_paper-841a0a56b573afb28da16f6650152fb4.pdf

### Communication
#### Telegram
#### Riot


Tezos Wiki

### Smart Contract
#### Michelson

#### Liquidity  
Announcing Liquidity version 1.0
http://www.ocamlpro.com/2019/03/08/announcing-liquidity-version-1-0/

#### Fi
Tutorials
 Getting Started With Fi: https://medium.com/coinmonks/getting-started-with-fi-a8eea75733db  
 Using Fi to Create A Voting Booth For Delegates - Part I: https://medium.com/@brice.aldrich/using-fi-to-create-a-voting-booth-for-delegates-part-i-f4772818af2d  
 Using Fi to Create A Voting Booth For Delegates — Part II: https://medium.com/@brice.aldrich/using-fi-to-create-a-voting-booth-for-delegates-part-ii-d92bec14e19  
 Creating a Tezos Auction — A Fi Tutorial: https://medium.com/@brice.aldrich/creating-a-tezos-auction-a-fi-tutorial-8d0e496dbe12  
 Tokenizing Real Estate With Fi: https://medium.com/@brice.aldrich/tokenizing-real-estate-with-fi-9e0a8c23ab51

#### smartPy
https://medium.com/@SmartPy_io/introducing-smartpy-and-smartpy-io-d4013bee7d4e  
https://medium.com/tocqueville-group/announcing-smartpy-fe9a62303c0b  
https://pypi.org/project/smartpy/

#### Ligo
featuring a Pascal-like syntax and a simple type system (by Nomadic Labs) for writing smart contracts for Layer-2 scaling, Marigold

Introducing LIGO: a new smart contract language for Tezos: https://medium.com/tezos/introducing-ligo-a-new-smart-contract-language-for-tezos-233fa17f21c7

Marigold: layer-2 scaling for Tezos
https://medium.com/tezos/marigold-layer-2-scaling-for-tezos-7445b5a3b7be

#### iOS
Swift libraries for Tezos blockchain  
TezosKit
https://github.com/keefertaylor/TezosKit  
TezosSwift
https://github.com/AckeeCZ/TezosSwift

Tutorials  
part1
https://www.ackee.cz/blog/en/interacting-with-the-tezos-blockchain-on-ios-part-1/
https://github.com/tezosrio  
part2
https://www.ackee.cz/blog/en/interacting-with-the-tezos-blockchain-on-ios-part-2/

#### Android
Android Java SDK for Tezos blockchian  
TezosJ_SDK
https://github.com/TezosRio/TezosJ_SDK  
Android Mobile Tezos Wallet  
TezzeT
https://github.com/TezosRio/TezzeT

#### Java
Java SDK for Tezos blockchain  
TezosJ_plainJava  
https://github.com/TezosRio/TezosJ_plainJava

#### Wallet and delegation
tezos wallets  
tutorials


#### Baker
tutorials

#### monitoring
#### baking slack
tezos-baking.slack.com

#### Automation of sending reward
backerei  
https://github.com/cryptiumlabs/backerei  
 introduction (for backerei)
  https://medium.com/cryptium/introducing-bäckerei-automated-rewards-payment-manager-for-tezos-bakers-written-in-haskell-ccead9599d3c

TAPS  
https://github.com/TezosRio/taps
