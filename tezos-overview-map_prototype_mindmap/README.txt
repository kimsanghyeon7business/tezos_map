A Pen created at CodePen.io. You can find this one at https://codepen.io/cewevii/pen/Lvewba.

 This strength/force-based mind map script is based on Kenneth Kufluk's http://kenneth.kufluk.com/ 2010 version.  I added size and color features.