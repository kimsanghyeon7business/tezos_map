

tz_overview_config_detail = {
   container: "#tz-overview-tree",
   siblingSeparation:   40,
   subTeeSeparation:    30,
   levelSeparation:     100,
   rootOrientation: "WEST",
   nodeAlign: "TOP",
   //animateOnInit: "false", //JQuery Required
   //animateOnInitDelay: "500"
};

root_tz_overview = {
  text: {name: "Tezos Overview Map"}  
};

node_doc = {
  parent: root_tz_overview,
  text: {name: "Document"}
};

node_doc_paper = {
  parent: node_doc,
  text: {name: "Paper"}
};

node_doc_wiki = {
  parent: node_doc,
  text: {name: "Wiki"}
};

node_tz_whitepaper  = {
  parent: node_doc_paper,
  text: { name: "White Paper" },
  link: { href: "https://tezos.com/static/white_paper-2dc8c02267a8fb86bd67a108199441bf.pdf", target: "_blank"}
};

node_tz_positionpaper = {
  parent: node_doc_paper,
  text: { name: "Position Paper"},
  link: { href:"https://tezos.com/static/position_paper-841a0a56b573afb28da16f6650152fb4.pdf", target: "_blank"},
};

node_tq_wiki = {
  parent: node_doc_wiki,
  text: { name: "TQ Tezos Wiki"},
  link: { href: "https://learn.tqgroup.io/", target: "_blank"}
};

node_communication = {
  parent: root_tz_overview,
  text: {name : "Communication"}
};

node_telegram = {
  parent: node_communication,
  text :{ name: "Telegram"}
};

node_telegram_tzofficial = {
  parent: node_telegram,
  text :{ name: "tezosofficial"},
  link :{ href: "https://t.me/tezosofficial", target: "_blank"}
};
  
node_telegram_tzplatform = {
  parent: node_telegram,
  text :{ name : "tezosplatform"},
  link :{ href : "https://t.me/tezosplatform", target:"_blank"}
};

node_telegram_tzico = {
  parent : node_telegram,
  text :{ name : "tezosico"},
  link :{ href : "https://t.me/tezosico", target:"_blank"}
};

node_telegram_tzkorea = {
  parent : node_telegram,
  text :{ name : "tezos_korea"},
  link :{ href : "https://t.me/tezos_korea", target:"_blank"}    
};

node_riot = {
  parent : node_communication,
  text :{ name: "Riot"},
  link: { href: "https://riot.im/app/#/room/#tezos:matrix.org", target: "_blank"}
};

node_slack = {
  parent : node_communication,
  text : { name: "Slack"},
};

node_slack_tz = {
  parent : node_slack,
  text : { name: "tezos"},
  link : { href: "https://tezos.slack.com/", target: "_blank"}
};

node_slack_tzbaker = {
  parent: node_slack,
  text: { name: "tezos-baker"},
  link : { href: "https://tezos-baking.slack.com/", target: "_blank"}
};

node_reddit = {
  parent : node_communication,
  text : { name: "Reddit"},
  link: { href: "https://www.reddit.com/r/tezos", target: "_blank"}
}

node_groups = {
  parent : root_tz_overview,
  text: { name: "Groups" }
};

node_tz_found = {
  parent : node_groups,
  text: {name : "Tezos Foundation"},
  link: { href: "https://tezos.foundation/", target: "_blank"}
};

node_tzcommons_found = {
  parent: node_groups,
  text: { name: "Tezos Commons Foundation"},
  link: { href: "https://tezoscommons.org/", target: "_blank"}
};

node_tzkorea = {
  parent: node_groups,
  text: { name: "Tezos Korea"},
  link: { href: "http://tezoskoreacommunity.org/", target: "_blank"}
};

node_tzkorea_found = {
  parent: node_groups,
  text: { name: "Tezos Korea Foundation"},
  link: { href: "http://tezoskorea.foundation/", target: "_blank"}
};

node_tzjapan = {
  parent: node_groups,
  text: { name: "Tezos Japan"},
  link: { href: "https://twitter.com/TezosJapan", target: "_blank"}
};

node_tzrio = {
  parent: node_groups,
  text: { name: "Tezos Rio"},
  link: { href: "https://tezos.rio/", target: "_blank"}
};
  
node_tzsea = {
  parent: node_groups,
  text: { name: "Tezos South East Asia"},
  link: { href: "https://www.tezos.org.sg/", target: "_blank"}
};

node_tzmontreal = {
  parent: node_groups,
  text: { name: "Tezos Montreal"},
  link: { href: "https://tezosmtl.com/", target: "_blank"}
};

node_tomi = {
  parent: node_groups,
  text: { name: "Tezos Ocaml Michelson Institute"},
  link: { href: "https://tomi.institute/", target: "_blank"}
};

node_tz_detroit = {
  parent: node_groups,
  text: { name: "Tezos Detroit"},
  link: { href: "https://twitter.com/TezosD", target: "_blank"}
};

node_tz_luxembourg = {
 parent: node_groups,
 text: { name: "Tezos Luxembourg"},
 link: { href: "https://tezos.lu/", target: "_blank"}
};

node_tz_istanbul = {
 parent: node_groups,
 text: { name: "Tezos Istanbul"},
 link: { href: "https://tezos.istanbul/", target: "_blank"}
};

node_tz_turkey = {
  parent: node_groups,
  text: { name: "Tezos Turkey"},
  link: { href: "https://twitter.com/tezosturkey", target: "_blank"}
};

node_tz_geneva = {
  parent: node_groups,
  text: { name: "Tezos Geneva"},
  link: { href: "https://twitter.com/TezosGeneva", target: "_blank"}
};

node_projects = {
  parent: root_tz_overview,
  text: { name: "Projects"}
};

node_project_develop = {
  parent: node_projects,
  text : { name : "Develop"}
};

node_project_bake = {
  parent: node_projects,
  text : { name : "Bake"}
};

node_project_wallet_delegate = {
  parent: node_projects,
  text : { name : "Wallet & Delegate"}
};

node_project_dapp = {
  parent: node_projects,
  text : { name : "DAPP"}
};

tz_overview_config = [
  tz_overview_config_detail, root_tz_overview, 
  
  node_doc, node_doc_paper, node_doc_wiki, 
  node_tz_whitepaper, node_tz_positionpaper, node_tq_wiki,
  
  node_communication, 
  node_telegram,  
  node_telegram_tzofficial, 
  node_telegram_tzplatform, node_telegram_tzico, node_telegram_tzkorea,
  node_riot,
  node_slack, node_slack_tz, node_slack_tzbaker,
  node_reddit,
  
  node_groups,
  
  node_tz_found,
  node_tomi,
  node_tzcommons_found, node_tzkorea, node_tzkorea_found, node_tzjapan, node_tzrio,
  node_tzsea,node_tzmontreal, node_tz_detroit, node_tz_luxembourg,
  node_tz_istanbul, node_tz_turkey, node_tz_geneva,
  
  node_projects, node_project_develop, node_project_bake, node_project_wallet_delegate, node_project_dapp
  
];

var tz_overview_tree = new Treant(tz_overview_config);

tz_dev_guide_config_detail = {
   container: "#tz-dev-guide-tree",
   siblingSeparation:   40,
   subTeeSeparation:    30,
   levelSeparation:     100,
   rootOrientation: "WEST",
   nodeAlign: "TOP",
   //animateOnInit: "false", //JQuery Required
   //animateOnInitDelay: "500"
};

root_tz_dev_guide = {
  text: {name: "Tezos Dev Guide"}  
};


tz_dev_guide_config = [
  tz_dev_guide_config_detail, root_tz_dev_guide
  /*node_smartcontract, node_scaling, node_sdk
  node_dev_sc_michelson, 
  
  node_dev_sc_liquidity,
  
  node_dev_sc_fi, 
  
  node_dev_sc_smartpy,
  
  node_dev_sc_ligo,*/
];

var tz_dev_guide_tree = new Treant(tz_dev_guide_config);

tz_baker_guide_config_detail = {
   container: "#tz-baker-guide-tree",
   siblingSeparation:   40,
   subTeeSeparation:    30,
   levelSeparation:     100,
   rootOrientation: "WEST",
   nodeAlign: "TOP",
   //animateOnInit: "false", //JQuery Required
   //animateOnInitDelay: "500"
};

root_tz_baker_guide = {
  text: {name: "Tezos Baker Guide"}  
};

/*
node_proejct_backerei = {
  parent: node_project_baker,
  text : {name : "backrei"},
  link: { href: "https://twitter.com/TezosGeneva", target: "_blank"}
}*/

tz_baker_guide_config = [
  tz_baker_guide_config_detail, root_tz_baker_guide

];

var tz_baker_guide_tree = new Treant(tz_baker_guide_config);

tz_user_guide_config_detail = {
   container: "#tz-user-guide-tree",
   siblingSeparation:   40,
   subTeeSeparation:    30,
   levelSeparation:     100,
   rootOrientation: "WEST",
   nodeAlign: "TOP",
   //animateOnInit: "false", //JQuery Required
   //animateOnInitDelay: "500"
};

root_tz_user_guide = {
  text: {name: "Tezos User Guide"}  
};
/*

*/
tz_user_guide_config = [
  tz_user_guide_config_detail, root_tz_user_guide
];

var tz_user_guide_tree = new Treant(tz_user_guide_config);